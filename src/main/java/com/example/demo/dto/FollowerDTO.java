package com.example.demo.dto;

import com.example.demo.entity.Follower;
import com.example.demo.utils.CONSTANTS;

import java.time.LocalDateTime;

public class FollowerDTO {
    private Long id;
    private Long userId;
    private Long followsOnId;
    private LocalDateTime createdAt;

    public FollowerDTO() {
    }

    public static FollowerDTO toDTO(Follower follower) {
        FollowerDTO followerDTO = new FollowerDTO();
        followerDTO.id = follower.getId();
        followerDTO.userId = follower.getUser().getId();
        followerDTO.followsOnId = follower.getFollowsOn().getId();
        followerDTO.setCreatedAt(follower.getCreatedAt());
        return followerDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt.format(CONSTANTS.FORMATTER);
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getFollowsOnId() {
        return followsOnId;
    }

    public void setFollowsOnId(Long followsOnId) {
        this.followsOnId = followsOnId;
    }
}
