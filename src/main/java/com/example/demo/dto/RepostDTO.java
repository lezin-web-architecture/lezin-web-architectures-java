package com.example.demo.dto;


import com.example.demo.entity.Repost;
import com.example.demo.utils.CONSTANTS;

import java.time.LocalDateTime;

public class RepostDTO {
    private Long id;
    private UserDTO user;
    private PostDTO post;

    private LocalDateTime createdAt;

    public static RepostDTO toDTO(Repost repost) {
        RepostDTO repostDTO = new RepostDTO();
        repostDTO.id = repost.getId();
        repostDTO.user = UserDTO.toModel(repost.getUser());
        repostDTO.post = PostDTO.toModel(repost.getPost());
        repostDTO.createdAt = repost.getCreatedAt();
        return repostDTO;
    }

    public RepostDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public PostDTO getPost() {
        return post;
    }

    public void setPost(PostDTO post) {
        this.post = post;
    }

    public String getCreatedAt() {
        return createdAt.format(CONSTANTS.FORMATTER);
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
