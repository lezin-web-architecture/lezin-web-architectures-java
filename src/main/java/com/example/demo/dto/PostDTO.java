package com.example.demo.dto;

import com.example.demo.entity.Like;
import com.example.demo.entity.Post;
import com.example.demo.entity.Repost;
import com.example.demo.utils.CONSTANTS;
import com.example.demo.utils.PostTypeEnum;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PostDTO {
    private Long id;
    private String text;
    private LocalDateTime createdAt;
    private UserDTO user;
    private PostTypeEnum type;
    private List<PostDTO> replies = new ArrayList<PostDTO>();
    private List<Long> likes = new ArrayList<>();
    private List<Long> reposts = new ArrayList<>();
    private UserDTO repostedUser;
    private LocalDateTime repostCreatedAt;

    private Long parentPostId;

    public static PostDTO toModel(Post post) {
        PostDTO postDTO = new PostDTO();
        postDTO.id = post.getId();
        postDTO.text = post.getText();
        postDTO.createdAt = post.getCreatedAt();
        postDTO.type = post.getType();
        postDTO.replies = post.getReplies().stream()
                .map(PostDTO::toModel)
                .collect(Collectors.toList());
        postDTO.likes = post.getLikes().stream().map(Like::getId).collect(Collectors.toList());
        postDTO.reposts = post.getReposts().stream().map(Repost::getId).collect(Collectors.toList());
        postDTO.user = UserDTO.toModel(post.getUser());
        if (post.getParentPost() != null) {
            postDTO.parentPostId = post.getParentPost().getId();
        }
        return postDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCreatedAt() {
        return createdAt.format(CONSTANTS.FORMATTER);
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public PostTypeEnum getType() {
        return type;
    }

    public void setType(PostTypeEnum type) {
        this.type = type;
    }

    public List<PostDTO> getReplies() {
        return replies;
    }

    public void setReplies(List<PostDTO> replies) {
        this.replies = replies;
    }

    public List<Long> getLikes() {
        return likes;
    }

    public void setLikes(List<Long> likes) {
        this.likes = likes;
    }

    public UserDTO getRepostedUser() {
        return repostedUser;
    }

    public void setRepostedUser(UserDTO repostedUser) {
        this.repostedUser = repostedUser;
    }

    public String getRepostCreatedAt() {
        if (repostCreatedAt == null) return null;
        return repostCreatedAt.format(CONSTANTS.FORMATTER);
    }

    public void setRepostCreatedAt(LocalDateTime createdAt) {
        this.repostCreatedAt = createdAt;
    }

    public List<Long> getReposts() {
        return reposts;
    }

    public void setReposts(List<Long> reposts) {
        this.reposts = reposts;
    }

    public Long getParentPostId() {
        return parentPostId;
    }

    public void setParentPostId(Long parentPostId) {
        this.parentPostId = parentPostId;
    }
}
