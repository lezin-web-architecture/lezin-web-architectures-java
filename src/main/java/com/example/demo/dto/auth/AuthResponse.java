package com.example.demo.dto.auth;

import com.example.demo.dto.UserDTO;

public class AuthResponse {
    private String token;

    private UserDTO user;

    public AuthResponse(String token, UserDTO userDTO) {
        this.token = token;
        this.user = userDTO;
    }

    public String getToken() {
        return token;
    }

    public UserDTO getUser() {
        return user;
    }
}