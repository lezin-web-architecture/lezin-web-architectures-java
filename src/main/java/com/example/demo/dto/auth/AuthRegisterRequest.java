package com.example.demo.dto.auth;

public class AuthRegisterRequest extends AuthLoginRequest{
    private String repeatedPassword;
    private String name;

    public String getRepeatedPassword() {
        return repeatedPassword;
    }

    public String getName() {
        return name;
    }
}
