package com.example.demo.dto;

import com.example.demo.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserDTO {
    private Long id;
    private String name;
    private String login;
    private String description;
    private List<Long> likes = new ArrayList<>();
    private List<Long> reposts = new ArrayList<>();

    private List<Long> followers = new ArrayList<>();

    private List<Long> followings = new ArrayList<>();

    public static UserDTO toModel(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.id = user.getId();
        userDTO.name = user.getName();
        userDTO.login = user.getLogin();
        userDTO.description = user.getDescription();
        return userDTO;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    public List<Long> getLikes() {
        return likes;
    }

    public void setLikes(List<Long> likes) {
        this.likes = likes;
    }

    public List<Long> getReposts() {
        return reposts;
    }

    public void setReposts(List<Long> reposts) {
        this.reposts = reposts;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Long> getFollowings() {
        return followings;
    }

    public void setFollowings(List<Long> followings) {
        this.followings = followings;
    }

    public List<Long> getFollowers() {
        return followers;
    }

    public void setFollowers(List<Long> followers) {
        this.followers = followers;
    }
}
