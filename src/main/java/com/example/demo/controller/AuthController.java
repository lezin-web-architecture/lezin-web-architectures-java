package com.example.demo.controller;

import com.example.demo.dto.UserDTO;
import com.example.demo.dto.auth.AuthLoginRequest;
import com.example.demo.dto.auth.AuthRegisterRequest;
import com.example.demo.dto.auth.AuthResponse;
import com.example.demo.dto.common.ResponseDTO;
import com.example.demo.entity.User;
import com.example.demo.excepion.user.NotFilledRequiredFieldsException;
import com.example.demo.excepion.user.UserAlreadyExistException;
import com.example.demo.excepion.user.UserNotFoundException;
import com.example.demo.service.UserService;
import com.example.demo.utils.JwtTokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Objects;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private UserService userService;
    @Autowired
    private JwtTokenUtils jwtTokenUtils;
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @PostMapping("/login")
    public ResponseEntity<ResponseDTO<AuthResponse>> login(@RequestBody AuthLoginRequest authLoginRequest) throws UserNotFoundException {
        if (authLoginRequest.getPassword() == null || authLoginRequest.getLogin() == null) {
            return ResponseEntity.ok(new ResponseDTO<>("Не заполнены все поля"));
        }
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            authLoginRequest.getLogin(), authLoginRequest.getPassword()
                    )
            );
        } catch (BadCredentialsException e) {
            return ResponseEntity.ok(new ResponseDTO("Неправильный логин или пароль"));
        }

        UserDetails userDetails = userService.loadUserByUsername(authLoginRequest.getLogin());
        User user = userService.findByLogin(authLoginRequest.getLogin());
        UserDTO userDTO = userService.getUserById(user.getId());
        String token = jwtTokenUtils.generateToken(userDetails);
        return ResponseEntity.ok(new ResponseDTO<>(new AuthResponse(token, userDTO)));
    }

    @PostMapping("/signup")
    public ResponseEntity<ResponseDTO<UserDTO>> signup(@RequestBody AuthRegisterRequest authRegisterRequest) throws UserAlreadyExistException, NotFilledRequiredFieldsException {
        try {
            if (authRegisterRequest.getPassword() == null
                    || authRegisterRequest.getRepeatedPassword() == null
                    || authRegisterRequest.getName() == null
                    || authRegisterRequest.getLogin() == null) {
                return ResponseEntity.ok(new ResponseDTO<>("Не заполнены все поля"));
            }
            if (!Objects.equals(authRegisterRequest.getPassword(), authRegisterRequest.getRepeatedPassword())) {
                return ResponseEntity.ok(new ResponseDTO<>("Пароли не совпадают"));
            }
            User user = new User();
            user.setPassword(passwordEncoder.encode(authRegisterRequest.getPassword()));
            user.setLogin(authRegisterRequest.getLogin());
            user.setName(authRegisterRequest.getName());
            UserDTO userDTO = userService.createUser(user);
            return ResponseEntity.ok(new ResponseDTO<>(userDTO));
        } catch (Exception e) {
            return ResponseEntity.ok(new ResponseDTO<>(e.getMessage()));
        }
    }

    @GetMapping("/me")
    public ResponseEntity<ResponseDTO<?>> getMe() throws UserNotFoundException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByLogin((String) authentication.getPrincipal());
        UserDTO userDTO = userService.getUserById(user.getId());
        String userLogin = (String) authentication.getPrincipal();

        UserDetails userDetails = userService.loadUserByUsername(userLogin);
        String token = jwtTokenUtils.generateToken(userDetails);
        return ResponseEntity.ok(new ResponseDTO<>(new AuthResponse(token, userDTO)));
    }

    @GetMapping("/logout")
    public ResponseEntity<ResponseDTO<?>> logout() {
        return ResponseEntity.ok(new ResponseDTO<>(new AuthResponse("LOGOUT_TOKEN", null)));
    }
}
