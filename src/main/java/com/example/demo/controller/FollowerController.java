package com.example.demo.controller;

import com.example.demo.dto.FollowerDTO;
import com.example.demo.dto.UserDTO;
import com.example.demo.dto.common.ResponseDTO;
import com.example.demo.entity.Follower;
import com.example.demo.excepion.follower.FollowerAlreadyExistException;
import com.example.demo.excepion.follower.FollowerNotFoundException;
import com.example.demo.excepion.user.NotFilledRequiredFieldsException;
import com.example.demo.service.FollowerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.ResponseEntity.*;

@RestController
@RequestMapping("/api/follow")
public class FollowerController {
    @Autowired
    private FollowerService followerService;

    @PostMapping
    public ResponseEntity<ResponseDTO<FollowerDTO>> createFollower(@RequestBody Follower follower) {
        try {
            return ok(new ResponseDTO<FollowerDTO>(followerService.createFollower(follower)));
        } catch (FollowerAlreadyExistException | NotFilledRequiredFieldsException e) {
            return ok(new ResponseDTO<>(e.getMessage()));
        }
    }

    @PostMapping("/unfollow")
    public ResponseEntity<ResponseDTO<FollowerDTO>> deleteFollower(@RequestBody Follower follower) {
        try {
            return ok(new ResponseDTO<FollowerDTO>(followerService.deleteFollower(follower)));
        } catch (FollowerNotFoundException | NotFilledRequiredFieldsException e) {
            return ok(new ResponseDTO<>(e.getMessage()));
        }
    }

    @GetMapping("/following-users/{userId}")
    public ResponseEntity<ResponseDTO<List<UserDTO>>> getAllFollowingUsers(@PathVariable Long userId) {
        try {
            return ok(new ResponseDTO<List<UserDTO>>(followerService.getFollowingUsers(userId)));
        } catch (Exception e) {
            return ok(new ResponseDTO<>(e.getMessage()));
        }
    }

    @GetMapping("/follower-users/{userId}")
    public ResponseEntity<ResponseDTO<List<UserDTO>>> getAllFollowerUsers(@PathVariable Long userId) {
        try {
            return ok(new ResponseDTO<List<UserDTO>>(followerService.getFollowerUsers(userId)));
        } catch (Exception e) {
            return ok(new ResponseDTO<>(e.getMessage()));
        }
    }
}
