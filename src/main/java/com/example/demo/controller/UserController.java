package com.example.demo.controller;

import com.example.demo.dto.UserDTO;
import com.example.demo.dto.common.ResponseDTO;
import com.example.demo.entity.User;
import com.example.demo.excepion.user.NotFilledRequiredFieldsException;
import com.example.demo.excepion.user.UserAlreadyExistException;
import com.example.demo.excepion.user.UserNotFoundException;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping
    public ResponseEntity<ResponseDTO<List<UserDTO>>> getUsers() {
        try {
            return ResponseEntity.ok(new ResponseDTO<>(userService.getUsers(), null));
        } catch (Exception e) {
            return ResponseEntity.ok(new ResponseDTO<>(null, "Произошла ошибка"));
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDTO<UserDTO>> getUserById(@PathVariable Long id) {
        try {
            System.out.println(new ResponseDTO<UserDTO>(userService.getUserById(id), null));
            return ResponseEntity.ok(new ResponseDTO<UserDTO>(userService.getUserById(id), null));
        } catch (UserNotFoundException e) {
            return ResponseEntity.ok(new ResponseDTO<UserDTO>(null, e.getMessage()));
        }
    }

    @PostMapping("/create")
    public ResponseEntity<ResponseDTO<UserDTO>> createUser(@RequestBody User user) {
        try {
            return ResponseEntity.ok(new ResponseDTO<>(userService.createUser(user), null));
        } catch (UserAlreadyExistException | NotFilledRequiredFieldsException e) {
            return ResponseEntity.ok(new ResponseDTO<>(null, e.getMessage()));
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ResponseDTO<UserDTO>> updateUser(@PathVariable Long id, @RequestBody User user) {
        try {
            return ResponseEntity.ok(new ResponseDTO<>(userService.updateUser(id, user), null));
        } catch (UserNotFoundException | NotFilledRequiredFieldsException e) {
            return ResponseEntity.ok(new ResponseDTO<>(null, e.getMessage()));
        }
    }

    @DeleteMapping("/remove/{id}")
    public ResponseEntity<ResponseDTO<UserDTO>> deleteUser(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(new ResponseDTO<>(userService.deleteUser(id), null));
        } catch (UserNotFoundException e) {
            return ResponseEntity.ok(new ResponseDTO<>(null, e.getMessage()));
        }
    }


//    @PostMapping("/signup")
//    public ResponseEntity signUp(@RequestBody User user) {
//        System.out.println(user.getName());
//        try {
//            User createdUser = userRepository.save(user);
//            return ResponseEntity.ok(createdUser);
//        } catch (Exception e) {
//            return null;
//        }
//    }
//
//    @PostMapping("/login")
//    public ResponseEntity logIn(@RequestBody User user) {
//        System.out.println(user);
//        return ResponseEntity.ok("hello");
//    }
}
