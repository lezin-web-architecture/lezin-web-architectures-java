package com.example.demo.controller;

import com.example.demo.dto.RepostDTO;
import com.example.demo.dto.common.ResponseDTO;
import com.example.demo.entity.Repost;
import com.example.demo.excepion.post.PostNotFoundException;
import com.example.demo.excepion.repost.RepostNotFoundException;
import com.example.demo.excepion.user.NotFilledRequiredFieldsException;
import com.example.demo.excepion.user.UserNotFoundException;
import com.example.demo.service.RepostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/reposts")
public class RepostController {

    @Autowired
    private RepostService repostService;

    @PostMapping
    public ResponseEntity<ResponseDTO<RepostDTO>> createRepost(@RequestBody Repost repost) {
        try {
            return ResponseEntity.ok(new ResponseDTO<RepostDTO>(repostService.createRepost(repost)));
        } catch (UserNotFoundException | PostNotFoundException e) {
            return ResponseEntity.ok(new ResponseDTO<>(e.getMessage()));
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDTO<Long>> deleteLike(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(new ResponseDTO<Long>(repostService.deleteLike(id)));
        } catch (NotFilledRequiredFieldsException | RepostNotFoundException e) {
            return ResponseEntity.ok(new ResponseDTO<>(e.getMessage()));
        }
    }
}
