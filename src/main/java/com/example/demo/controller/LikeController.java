package com.example.demo.controller;

import com.example.demo.dto.common.ResponseDTO;
import com.example.demo.entity.Like;
import com.example.demo.excepion.like.LikeNotFoundException;
import com.example.demo.excepion.post.PostNotFoundException;
import com.example.demo.excepion.user.NotFilledRequiredFieldsException;
import com.example.demo.excepion.user.UserNotFoundException;
import com.example.demo.service.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/likes")
public class LikeController {
    @Autowired
    LikeService likeService;

    @PostMapping
    public ResponseEntity<ResponseDTO<Long>> createLike(@RequestBody Like like) {
        try {
            return ResponseEntity.ok(new ResponseDTO<Long>(likeService.createLike(like)));
        } catch (UserNotFoundException | PostNotFoundException e) {
            return ResponseEntity.ok(new ResponseDTO(e.getMessage()));
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDTO<Long>> deleteLike(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(new ResponseDTO<Long>(likeService.deleteLike(id)));
        } catch (NotFilledRequiredFieldsException | LikeNotFoundException e) {
            return ResponseEntity.ok(new ResponseDTO<>(e.getMessage()));
        }
    }
}
