package com.example.demo.controller;

import com.example.demo.dto.PostDTO;
import com.example.demo.dto.common.ResponseDTO;
import com.example.demo.entity.Post;
import com.example.demo.excepion.post.PostNotFoundException;
import com.example.demo.excepion.user.NotFilledRequiredFieldsException;
import com.example.demo.excepion.user.UserNotFoundException;
import com.example.demo.service.PostService;
import com.example.demo.utils.PostQueryParams;
import com.example.demo.utils.SortTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/posts")
public class PostController {

    @Autowired
    private PostService postService;

    @PostMapping("/create")
    public ResponseEntity<ResponseDTO<PostDTO>> createPost(
            @RequestBody Post post
    ) {
        try {
            return ResponseEntity.ok(new ResponseDTO<>(postService.createPost(post)));
        } catch (NotFilledRequiredFieldsException | UserNotFoundException | PostNotFoundException e) {
            return ResponseEntity.ok(new ResponseDTO<>(e.getMessage()));
        }
    }

    @GetMapping
    public ResponseEntity<ResponseDTO<List<PostDTO>>> getAllPosts(
            @RequestParam(required = false, defaultValue = "DESC") SortTypeEnum sortType
    ) {
        try {
            return ResponseEntity.ok(new ResponseDTO<>(
                    postService.getAllPosts(new PostQueryParams.Builder()
                            .sortType(sortType)
                            .build()
                    )
            ));
        } catch (Exception e) {
            return ResponseEntity.ok(new ResponseDTO<>(e.getMessage()));
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDTO<PostDTO>> getPostById(
            @PathVariable Long id
    ) {
        try {
            return ResponseEntity.ok(new ResponseDTO<>(postService.getPostById(id)));
        } catch (Exception e) {
            return ResponseEntity.ok(new ResponseDTO<>(e.getMessage()));
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDTO<Long>> removePost(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(new ResponseDTO<>(postService.removePost(id)));
        } catch (PostNotFoundException e) {
            return ResponseEntity.ok(new ResponseDTO<>(e.getMessage()));
        }
    }
}
