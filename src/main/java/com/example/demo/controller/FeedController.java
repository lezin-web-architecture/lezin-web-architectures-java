package com.example.demo.controller;

import com.example.demo.dto.PostDTO;
import com.example.demo.dto.common.ResponseDTO;
import com.example.demo.service.FeedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/feed")
public class FeedController {
    @Autowired
    private FeedService feedService;

    @GetMapping
    public ResponseEntity<ResponseDTO<List<PostDTO>>> getFeed() {
        try {
            return ResponseEntity.ok(new ResponseDTO<List<PostDTO>>(feedService.getFeed()));
        } catch (Exception e) {
            return ResponseEntity.ok(new ResponseDTO<List<PostDTO>>(e.getMessage()));
        }
    }

    @GetMapping("/{userId}")
    public ResponseEntity<ResponseDTO<List<PostDTO>>> getAllPostsByUserId(@PathVariable Long userId) {
        try {
            return ResponseEntity.ok(new ResponseDTO<>(feedService.getFeedByUserId(userId)));
        } catch (Exception e) {
            return ResponseEntity.ok(new ResponseDTO<>(e.getMessage()));
        }
    }

    @GetMapping("/replies/{userId}")
    public ResponseEntity<ResponseDTO<List<PostDTO>>> getAllRepliesByUserId(@PathVariable Long userId) {
        try {
            return ResponseEntity.ok(new ResponseDTO<>(feedService.getFeedOfRepliesByUserId(userId)));
        } catch (Exception e) {
            return ResponseEntity.ok(new ResponseDTO<>(e.getMessage()));
        }
    }

    @GetMapping("/likes/{userId}")
    public ResponseEntity<ResponseDTO<List<PostDTO>>> getAllLikedPostsByUserId(@PathVariable Long userId) {
        try {
            return ResponseEntity.ok(new ResponseDTO<>(feedService.getFeedOfLikedPostsByUserId(userId)));
        } catch (Exception e) {
            return ResponseEntity.ok(new ResponseDTO<>(e.getMessage()));
        }
    }
}
