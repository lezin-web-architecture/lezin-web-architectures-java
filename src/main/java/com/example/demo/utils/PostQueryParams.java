package com.example.demo.utils;

public class PostQueryParams {
    private Long userId;
    private SortTypeEnum sortType;

    public PostQueryParams(Long userId, SortTypeEnum sortType) {
        this.userId = userId;
        this.sortType = sortType;
    }
    public PostQueryParams(SortTypeEnum sortType) {
        this.sortType = sortType;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public SortTypeEnum getSortType() {
        return sortType;
    }

    public void setSortType(SortTypeEnum sortType) {
        this.sortType = sortType;
    }


    public static class Builder {
        private Long userId;
        private SortTypeEnum sortType;

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder sortType(SortTypeEnum sortType) {
            this.sortType = sortType;
            return this;
        }

        public PostQueryParams build() {
            return new PostQueryParams(userId, sortType);
        }
    }
}
