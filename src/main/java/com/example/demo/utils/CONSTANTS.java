package com.example.demo.utils;

import java.time.format.DateTimeFormatter;

public class CONSTANTS {
    public static String DATE_STRING_FORMAT =  "d MMM yy'г.' HH:mm:ss";
    public static DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(DATE_STRING_FORMAT);
}
