package com.example.demo.repository;

import com.example.demo.entity.Follower;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FollowerRepo extends CrudRepository<Follower, Long> {
    Follower findByUserIdAndFollowsOnId(Long followerId, Long followingId);
    @Query("SELECT f.followsOn.id from Follower f WHERE f.user.id = :userId")
    List<Long> findAllFollowsOnIdByUserId(Long userId);

    @Query("SELECT f.user.id from Follower f WHERE f.followsOn.id = :followsOnId")
    List<Long> findAllUserIdByFollowsOnId(Long followsOnId);
}
