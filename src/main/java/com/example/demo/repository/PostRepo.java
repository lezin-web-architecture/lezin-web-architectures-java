package com.example.demo.repository;

import com.example.demo.entity.Post;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PostRepo extends CrudRepository<Post, Long> {
    List<Post> findAllByUserId(Long id);

    @Query("SELECT p FROM Post p WHERE p.user.id = :userId AND p.type = 1")
    List<Post> findAllRepliesByUserId(Long userId);

    @Query("SELECT l.post FROM Like l WHERE l.user.id = :userId")
    List<Post> findAllLikedPostsByUserId(Long userId);

    @Override
    List<Post> findAll();
}
