package com.example.demo.repository;

import com.example.demo.entity.Repost;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RepostRepo extends CrudRepository<Repost, Long> {
    List<Repost> findAll();

    List<Repost> findAllByUserId(Long userId);

    @Query("SELECT r.post.id FROM Repost r WHERE r.user.id = :userId")
    List<Long> findAllRepostedPostIdsByUserId(Long userId);

    @Query("SELECT r.id FROM Repost r WHERE r.user.id = :userId")
    List<Long> findAllRepostsIdsByUserId(Long userId);
}
