package com.example.demo.repository;

import com.example.demo.entity.Like;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LikeRepo extends CrudRepository<Like, Long> {
    @Query("SELECT l.id FROM Like l WHERE user.id = :userId")
    List<Long> findAllLikesIdByUserId(Long userId);
}
