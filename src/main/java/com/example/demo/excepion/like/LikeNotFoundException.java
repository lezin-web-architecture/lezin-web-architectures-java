package com.example.demo.excepion.like;

public class LikeNotFoundException extends Exception {
    public LikeNotFoundException(String message) {
        super("Лайка с таким " + message + " не существует");
    }
}
