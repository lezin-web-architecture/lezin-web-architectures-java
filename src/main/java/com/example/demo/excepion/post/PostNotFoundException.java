package com.example.demo.excepion.post;

public class PostNotFoundException extends Exception {
    public PostNotFoundException(String message) {
        super("Поста с таким " + message + " не существует");
    }
}
