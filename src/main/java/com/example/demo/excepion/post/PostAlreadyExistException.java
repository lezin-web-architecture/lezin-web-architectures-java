package com.example.demo.excepion.post;

public class PostAlreadyExistException extends Exception {
    public PostAlreadyExistException(String message) {
        super("Пост с таким " + message + " уже существует");
    }
}
