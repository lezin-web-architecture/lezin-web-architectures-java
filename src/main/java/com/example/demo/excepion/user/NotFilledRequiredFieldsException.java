package com.example.demo.excepion.user;

public class NotFilledRequiredFieldsException extends Exception{
    public NotFilledRequiredFieldsException(String message) {
        super("Не заполнены обязательные поля" + message);
    }
}
