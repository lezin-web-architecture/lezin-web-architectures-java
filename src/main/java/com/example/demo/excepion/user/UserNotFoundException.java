package com.example.demo.excepion.user;

public class UserNotFoundException extends Exception {
    public UserNotFoundException(String message) {
        super("Пользователя с таким " + message + " не существует");
    }
}
