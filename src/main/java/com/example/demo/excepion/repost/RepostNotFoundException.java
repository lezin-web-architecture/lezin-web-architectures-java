package com.example.demo.excepion.repost;

public class RepostNotFoundException extends Exception {
    public RepostNotFoundException(String message) {
        super("Репоста с таким " + message + " не существует");
    }
}
