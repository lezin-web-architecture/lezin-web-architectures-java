package com.example.demo.excepion.follower;

public class FollowerAlreadyExistException extends Exception {
    public FollowerAlreadyExistException(String message) {
        super("Подписчик/подписка с таким " + message + " уже существует");
    }
}
