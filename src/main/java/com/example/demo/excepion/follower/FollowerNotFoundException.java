package com.example.demo.excepion.follower;

public class FollowerNotFoundException extends Exception {
    public FollowerNotFoundException(String message) {
        super("Подписчика с таким " + message + " не существует");
    }
}
