package com.example.demo.service;

import com.example.demo.dto.PostDTO;
import com.example.demo.dto.RepostDTO;
import com.example.demo.utils.CONSTANTS;
import com.example.demo.utils.PostQueryParams;
import com.example.demo.utils.PostTypeEnum;
import com.example.demo.utils.SortTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FeedService {
    @Autowired
    private PostService postService;

    @Autowired
    private RepostService repostService;

    public List<PostDTO> getFeed() {
        List<PostDTO> posts = postService.getAllPosts(new PostQueryParams(SortTypeEnum.DESC))
                .stream().filter(p -> p.getType().equals(PostTypeEnum.POST)).toList();
        List<RepostDTO> reposts = repostService.getAllReposts();

        List<PostDTO> feedList = new ArrayList<>();
        feedList.addAll(posts);
        feedList.addAll(this.getAllPostsOfReposts(reposts));

        feedList.sort((post1, post2) -> {
            LocalDateTime date1 = LocalDateTime.parse((post1.getRepostCreatedAt() != null) ? post1.getRepostCreatedAt() : post1.getCreatedAt(), CONSTANTS.FORMATTER);
            LocalDateTime date2 = LocalDateTime.parse((post2.getRepostCreatedAt() != null) ? post2.getRepostCreatedAt() : post2.getCreatedAt(), CONSTANTS.FORMATTER);
            return date2.compareTo(date1);
        });
        return feedList;
    }

    public List<PostDTO> getFeedByUserId(Long userId) {
        List<PostDTO> posts = postService.getAllPostsByUserId(userId)
                .stream().filter(p -> p.getType().equals(PostTypeEnum.POST)).toList();
        List<RepostDTO> reposts = repostService.getAllRepostsByUserId(userId);

        List<PostDTO> feedList = new ArrayList<>();
        feedList.addAll(posts);
        feedList.addAll(this.getAllPostsOfReposts(reposts));

        feedList.sort((post1, post2) -> {
            LocalDateTime date1 = LocalDateTime.parse((post1.getRepostCreatedAt() != null) ? post1.getRepostCreatedAt() : post1.getCreatedAt(), CONSTANTS.FORMATTER);
            LocalDateTime date2 = LocalDateTime.parse((post2.getRepostCreatedAt() != null) ? post2.getRepostCreatedAt() : post2.getCreatedAt(), CONSTANTS.FORMATTER);
            return date2.compareTo(date1);
        });
        return feedList;
    }

    public List<PostDTO> getFeedOfRepliesByUserId(Long userId) {
        List<PostDTO> posts = postService.getAllRepliesByUserId(userId);
        List<PostDTO> feedList = new ArrayList<>(posts);
        feedList.sort((post1, post2) -> {
            LocalDateTime date1 = LocalDateTime.parse((post1.getRepostCreatedAt() != null) ? post1.getRepostCreatedAt() : post1.getCreatedAt(), CONSTANTS.FORMATTER);
            LocalDateTime date2 = LocalDateTime.parse((post2.getRepostCreatedAt() != null) ? post2.getRepostCreatedAt() : post2.getCreatedAt(), CONSTANTS.FORMATTER);
            return date2.compareTo(date1);
        });
        return feedList;

    }

    public List<PostDTO> getFeedOfLikedPostsByUserId(Long userId) {
        List<PostDTO> posts = postService.getAllLikedPostsByUserId(userId);
        List<PostDTO> feedList = new ArrayList<>(posts);
        feedList.sort((post1, post2) -> {
            LocalDateTime date1 = LocalDateTime.parse((post1.getRepostCreatedAt() != null) ? post1.getRepostCreatedAt() : post1.getCreatedAt(), CONSTANTS.FORMATTER);
            LocalDateTime date2 = LocalDateTime.parse((post2.getRepostCreatedAt() != null) ? post2.getRepostCreatedAt() : post2.getCreatedAt(), CONSTANTS.FORMATTER);
            return date2.compareTo(date1);
        });
        return feedList;

    }

    private List<PostDTO> getAllPostsOfReposts(List<RepostDTO> repostDTOs) {
        return repostDTOs.stream().map(repostDTO -> {
            PostDTO post = repostDTO.getPost();
            post.setRepostedUser(repostDTO.getUser());
            post.setRepostCreatedAt(LocalDateTime.parse(repostDTO.getCreatedAt(), CONSTANTS.FORMATTER));
            return post;
        }).collect(Collectors.toList());
    }
}
