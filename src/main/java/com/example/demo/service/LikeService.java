package com.example.demo.service;

import com.example.demo.entity.Like;
import com.example.demo.entity.Post;
import com.example.demo.entity.User;
import com.example.demo.excepion.like.LikeNotFoundException;
import com.example.demo.excepion.post.PostNotFoundException;
import com.example.demo.excepion.user.NotFilledRequiredFieldsException;
import com.example.demo.excepion.user.UserNotFoundException;
import com.example.demo.repository.LikeRepo;
import com.example.demo.repository.PostRepo;
import com.example.demo.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LikeService {

    @Autowired
    LikeRepo likeRepo;
    @Autowired
    UserRepo userRepo;
    @Autowired
    PostRepo postRepo;

    public Long createLike(Like like) throws UserNotFoundException, PostNotFoundException {
        User user = null;
        if (like.getUser() != null && like.getUser().getId() != null) {
            user = userRepo.findById(like.getUser().getId()).orElse(null);
        }
        Post post = null;
        if (like.getUser() != null && like.getUser().getId() != null) {
            post = postRepo.findById(like.getPost().getId()).orElse(null);
        }

        if (user == null) throw new UserNotFoundException("ID");
        if (post == null) throw new PostNotFoundException("ID");

        like.setUser(user);
        like.setPost(post);
        likeRepo.save(like);
        return like.getId();
    }

    public Long deleteLike(Long id) throws NotFilledRequiredFieldsException, LikeNotFoundException {
        if (id == null) {
            throw new NotFilledRequiredFieldsException(": ID");
        }
        if (!likeRepo.existsById(id)) {
            throw new LikeNotFoundException("ID");
        }

        likeRepo.deleteById(id);
        return id;
    }
}
