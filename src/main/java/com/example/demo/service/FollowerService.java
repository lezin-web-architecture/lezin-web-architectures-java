package com.example.demo.service;

import com.example.demo.dto.FollowerDTO;
import com.example.demo.dto.UserDTO;
import com.example.demo.entity.Follower;
import com.example.demo.entity.User;
import com.example.demo.excepion.follower.FollowerAlreadyExistException;
import com.example.demo.excepion.follower.FollowerNotFoundException;
import com.example.demo.excepion.user.NotFilledRequiredFieldsException;
import com.example.demo.repository.FollowerRepo;
import com.example.demo.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FollowerService {
    @Autowired
    private FollowerRepo followerRepo;

    @Autowired
    private UserRepo userRepo;

    public FollowerDTO createFollower(Follower follower) throws NotFilledRequiredFieldsException, FollowerAlreadyExistException {
        User user = follower.getUser();
        User followsOnUser = follower.getFollowsOn();

        if ((user == null || user.getId() == null)
                || (followsOnUser == null || followsOnUser.getId() == null)) {
            throw new NotFilledRequiredFieldsException(": followerId или followingId");
        }

        if (followerRepo.findByUserIdAndFollowsOnId(user.getId(), followsOnUser.getId()) != null) {
            throw new FollowerAlreadyExistException("followerId и followingId");
        }

        follower.setCreatedAt(LocalDateTime.now());
        return FollowerDTO.toDTO(followerRepo.save(follower));
    }

    public FollowerDTO deleteFollower(Follower follower) throws NotFilledRequiredFieldsException, FollowerNotFoundException {
        User user = follower.getUser();
        User followsOnUser = follower.getFollowsOn();
        if ((user == null || user.getId() == null)
                || (followsOnUser == null || followsOnUser.getId() == null)) {
            throw new NotFilledRequiredFieldsException(": followerId или followingId");
        }

        Follower followerForDelete = followerRepo.findByUserIdAndFollowsOnId(user.getId(), followsOnUser.getId());
        if (followerForDelete == null) {
            throw new FollowerNotFoundException("followerId и followingId");
        }

        followerRepo.deleteById(followerForDelete.getId());
        return FollowerDTO.toDTO(followerForDelete);
    }

    public List<UserDTO> getFollowingUsers(Long userId) {
        List<Long> followingUsersIds = followerRepo.findAllFollowsOnIdByUserId(userId);
        List<User> users = (List<User>) userRepo.findAllById(followingUsersIds);
        return users.stream().map(UserDTO::toModel).collect(Collectors.toList());
    }

    public List<UserDTO> getFollowerUsers(Long userId) {
        List<Long> followerUsersIds = followerRepo.findAllUserIdByFollowsOnId(userId);
        List<User> users = (List<User>) userRepo.findAllById(followerUsersIds);
        return users.stream().map(UserDTO::toModel).collect(Collectors.toList());
    }
}
