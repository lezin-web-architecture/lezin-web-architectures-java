package com.example.demo.service;

import com.example.demo.dto.RepostDTO;
import com.example.demo.entity.Post;
import com.example.demo.entity.Repost;
import com.example.demo.entity.User;
import com.example.demo.excepion.post.PostNotFoundException;
import com.example.demo.excepion.repost.RepostNotFoundException;
import com.example.demo.excepion.user.NotFilledRequiredFieldsException;
import com.example.demo.excepion.user.UserNotFoundException;
import com.example.demo.repository.PostRepo;
import com.example.demo.repository.RepostRepo;
import com.example.demo.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class RepostService {
    @Autowired
    private RepostRepo repostRepo;

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private PostRepo postRepo;

    public RepostDTO createRepost(Repost repost) throws UserNotFoundException, PostNotFoundException {
        User user = null;
        if (repost.getUser() != null && repost.getUser().getId() != null) {
            user = userRepo.findById(repost.getUser().getId()).orElse(null);
        }
        Post post = null;
        if (repost.getUser() != null && repost.getUser().getId() != null) {
            post = postRepo.findById(repost.getPost().getId()).orElse(null);
        }

        if (user == null) throw new UserNotFoundException("ID");
        if (post == null) throw new PostNotFoundException("ID");

        repost.setUser(user);
        repost.setPost(post);
        repost.setCreatedAt(LocalDateTime.now());
        return RepostDTO.toDTO(repostRepo.save(repost));
    }

    public Long deleteLike(Long id) throws NotFilledRequiredFieldsException, RepostNotFoundException {
        if (id == null) {
            throw new NotFilledRequiredFieldsException(": ID");
        }
        if (!repostRepo.existsById(id)) {
            throw new RepostNotFoundException("ID");
        }

        repostRepo.deleteById(id);
        return id;
    }

    public List<RepostDTO> getAllReposts() {
        List<Repost> reposts = repostRepo.findAll();
        List<RepostDTO> repostDTOs = reposts.stream().map(RepostDTO::toDTO).toList();
        return repostDTOs;
    }

    public List<RepostDTO> getAllRepostsByUserId(Long userId) {
        List<Repost> reposts = repostRepo.findAllByUserId(userId);
        List<RepostDTO> repostDTOs = reposts.stream().map(RepostDTO::toDTO).toList();
        return repostDTOs;
    }
}
