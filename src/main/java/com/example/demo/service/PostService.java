package com.example.demo.service;

import com.example.demo.dto.PostDTO;
import com.example.demo.dto.UserDTO;
import com.example.demo.entity.Post;
import com.example.demo.entity.User;
import com.example.demo.excepion.post.PostNotFoundException;
import com.example.demo.excepion.user.NotFilledRequiredFieldsException;
import com.example.demo.excepion.user.UserNotFoundException;
import com.example.demo.repository.PostRepo;
import com.example.demo.repository.UserRepo;
import com.example.demo.utils.PostQueryParams;
import com.example.demo.utils.PostTypeEnum;
import com.example.demo.utils.SortTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PostService {

    @Autowired
    private PostRepo postRepo;

    @Autowired
    private UserRepo userRepo;

    public PostDTO createPost(Post post) throws UserNotFoundException, NotFilledRequiredFieldsException, PostNotFoundException {
        if (post.getUser() == null || post.getUser().getId() == null) {
            throw new NotFilledRequiredFieldsException(": userId");
        }

        User user = userRepo.findById(post.getUser().getId()).orElse(null);

        if (user == null) {
            throw new UserNotFoundException("ID");
        }

        if (post.getText() == null || post.getText() == "") {
            throw new NotFilledRequiredFieldsException(": text");
        }

        if (post.getParentPost() != null && post.getParentPost().getId() != null) {
            Post parentPost = postRepo.findById(post.getParentPost().getId()).orElse(null);
            if (parentPost == null) {
                throw new PostNotFoundException("ID");
            }
            post.setParentPost(parentPost);
            post.setType(PostTypeEnum.REPLY);
        }


        post.setUser(user);
        post.setCreatedAt(LocalDateTime.now());
        if (post.getType() == null || !post.getType().equals(PostTypeEnum.REPLY)) {
            post.setType(PostTypeEnum.POST);
        }
        return PostDTO.toModel(postRepo.save(post));
    }

    public List<PostDTO> getAllPosts(PostQueryParams postQueryParams) {
        List<Post> posts = postRepo.findAll();

        if (postQueryParams.getSortType().equals(SortTypeEnum.DESC)) {
            Collections.reverse(posts);
        }

        return posts.stream().map(PostDTO::toModel).collect(Collectors.toList());
    }

    public PostDTO getPostById(Long id) throws PostNotFoundException {
        Optional<Post> post = postRepo.findById(id);
        if (post.isEmpty()) {
            throw new PostNotFoundException("ID");
        }

        return PostDTO.toModel(post.get());
    }

    public Long removePost(Long id) throws PostNotFoundException {
        if (!postRepo.existsById(id)) {
            throw new PostNotFoundException("ID");
        }
        postRepo.deleteById(id);
        return id;
    }

    public List<PostDTO> getAllPostsByUserId(Long userId) {
        List<Post> posts = postRepo.findAllByUserId(userId);

        Collections.reverse(posts);
        return posts.stream().map(PostDTO::toModel).collect(Collectors.toList());
    }

    public List<PostDTO> getAllRepliesByUserId(Long userId) {
        List<Post> posts = postRepo.findAllRepliesByUserId(userId);
        Collections.reverse(posts);
        return posts.stream().map(PostDTO::toModel).collect(Collectors.toList());
    }

    public List<PostDTO> getAllLikedPostsByUserId(Long userId) {
        List<Post> posts = postRepo.findAllLikedPostsByUserId(userId);
        Collections.reverse(posts);
        return posts.stream().map(PostDTO::toModel).collect(Collectors.toList());
    }
}
