package com.example.demo.service;

import com.example.demo.dto.UserDTO;
import com.example.demo.entity.User;
import com.example.demo.excepion.user.NotFilledRequiredFieldsException;
import com.example.demo.excepion.user.UserAlreadyExistException;
import com.example.demo.excepion.user.UserNotFoundException;
import com.example.demo.repository.FollowerRepo;
import com.example.demo.repository.LikeRepo;
import com.example.demo.repository.RepostRepo;
import com.example.demo.repository.UserRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepo userRepo;

    @Autowired
    private LikeRepo likeRepo;

    @Autowired
    private RepostRepo repostRepo;

    @Autowired
    FollowerRepo followerRepo;

    public List<UserDTO> getUsers() {
        return StreamSupport.stream(userRepo.findAll().spliterator(), false)
                .map(UserDTO::toModel).collect(Collectors.toList());
    }

    public UserDTO getUserById(Long id) throws UserNotFoundException {
        User user = userRepo.findById(id).orElse(null);
        if (user == null) {
            throw new UserNotFoundException("ID");
        }
        List<Long> userLikes = likeRepo.findAllLikesIdByUserId(user.getId());
        List<Long> userRepostedRepostIds = repostRepo.findAllRepostsIdsByUserId(user.getId());
        UserDTO userDTO = UserDTO.toModel(user);
        userDTO.setFollowings(followerRepo.findAllFollowsOnIdByUserId(userDTO.getId()));
        userDTO.setFollowers(followerRepo.findAllUserIdByFollowsOnId(userDTO.getId()));
        userDTO.setLikes(userLikes);
        userDTO.setReposts(userRepostedRepostIds);
        return userDTO;
    }

    public UserDTO createUser(User user) throws UserAlreadyExistException, NotFilledRequiredFieldsException {
        if (user.getLogin() == null || user.getName() == null || user.getPassword() == null) {
            throw new NotFilledRequiredFieldsException("");
        }
        User findedUser = userRepo.findByLogin(user.getLogin());
        if (findedUser != null) {
            throw new UserAlreadyExistException("login = " + findedUser.getLogin());
        }

        return UserDTO.toModel(userRepo.save(user));
    }

    public UserDTO updateUser(Long id, User user) throws UserNotFoundException, NotFilledRequiredFieldsException {
        Optional<User> findedUser = userRepo.findById(id);
        if (findedUser.isEmpty()) {
            throw new UserNotFoundException("ID");
        } else {
            if (user.getLogin() == null || user.getName() == null) {
                throw new NotFilledRequiredFieldsException("");
            }
            BeanUtils.copyProperties(user, findedUser.get(), "id");
            return UserDTO.toModel(userRepo.save(findedUser.get()));
        }
    }

    public UserDTO deleteUser(Long id) throws UserNotFoundException {
        Optional<User> userForRemove = userRepo.findById(id);
        if (userForRemove.isEmpty()) {
            throw new UserNotFoundException("ID");
        }
        userRepo.deleteById(id);
        return UserDTO.toModel(userForRemove.get());
    }

    public User findByLogin(String login) {
        User user = userRepo.findByLogin(login);
        if (user == null) {
            throw new UsernameNotFoundException(String.format("Пользователь '%s' не найден", login));
        }
        return user;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = findByLogin(username);
        return new org.springframework.security.core.userdetails.User(
                user.getLogin(),
                user.getPassword(),
                Collections.emptyList()
        );
    }
}
